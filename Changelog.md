# Changelog
### Los cambios notables de este proyecto seran documentados en este archivo.
-------------------
## [v1.4]
### Añadido
- MundoServidor.cpp y MundoServidor.h
- MundoCliente.cpp y MundoCliente.h

### Cambiado


### Eliminado
- Mundo

-------------------
## [v1.3]
### Añadido
- bot.cpp funcionando
- logger.cpp funcionando
- Carpeta /tmp

### Cambiado
- Cambios en Mundo y Plano para implementar lo anterior

### Eliminado
-------------------
## [v1.2]
### Añadido
- Changelog.md a la carpeta sii_53399/

### Cambiado
- Clase Esfera: Añadido movimiento y reducción de tamaño
- Clase Raqueta: Añadido movimiento

### Eliminado
- Changelog.txt de la carpeta sii_53399/workspace/

---------------------
## [v1.1]
### Añadido
- Changelog
 
### Cambiado
- Cabeceras de los archivos con una etiqueta con el nombre

### Eliminado

---------------------  
## [v1.0]
### Añadido
- Se ha hecho el fork y se ha clonado el repositorio localmente.

### Cambiado

### Eliminado
