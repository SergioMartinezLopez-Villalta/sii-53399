// Mundo.h: interface for the CMundo class.
// SMLV
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include "Esfera.h"
//#include "Raqueta.h"
#include "DatosMemCompartida.h"

class CMundoServidor  
{
public:
	void Init();
	CMundoServidor();
	virtual ~CMundoServidor();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	
	void RecibeComandosJugador();

	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;

	int fd; //Descriptor de la Fifo
	int fd_servidor_cliente;
	int fd_cliente_servidor;
	
	pthread_t thid1;
	pthread_attr_t atrib;


	int puntos1;
	int puntos2;
	int ganador; //Para saber quien gano al pasarlo a la Fifo
	
	int puntuacion[2]; //Para pasar puntos y ganador por la Fifo

	DatosMemCompartida datosComp;
	DatosMemCompartida *pdatosComp;
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
