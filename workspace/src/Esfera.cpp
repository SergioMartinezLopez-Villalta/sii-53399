// Esfera.cpp: implementation of the Esfera class.
// Sergio Martinez Lopez-Villalta
//////////////////////////////////////////////////////////////////////

#include "Esfera.h"
#include "glut.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Esfera::Esfera()
{
	radio=0.5f;
	velocidad.x=3;
	velocidad.y=3;
}

Esfera::~Esfera()
{

}



void Esfera::Dibuja()
{
	glColor3ub(255,255,0);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	glutSolidSphere(radio,15,15);
	glPopMatrix();
}

void Esfera::Mueve(float t)
{
	if (radio>0.1f)
	radio=radio-t*0.05f;
	
	centro.x=centro.x+velocidad.x*0.05f;
	centro.y=centro.y+velocidad.y*0.05f;
}
