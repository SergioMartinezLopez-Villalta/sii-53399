// Mundo.cpp: implementation of the CMundo class.
// Sergio Martinez Lopez-Villalta
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <stdio.h>
#include <sys/types.h> 
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundoCliente::CMundoCliente()
{
	Init();
}

CMundoCliente::~CMundoCliente()
{

	munmap(pdatosComp, sizeof(datosComp));
	unlink("/tmp/datosComp");
	close(fd_servidor_cliente);
	unlink("/tmp/fifo_servidor_cliente");
	close(fd_cliente_servidor);
	unlink("/tmp/fifo_cliente_servidor");
}

void CMundoCliente::InitGL()
{
	

	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundoCliente::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundoCliente::OnTimer(int value)
{	
	char cad[200];
	
	read(fd_servidor_cliente,cad,sizeof(cad));

	sscanf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d", &esfera.centro.x,&esfera.centro.y, &jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2, &jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2, &puntos1, &puntos2); 


	pdatosComp->raqueta1=jugador1;
	pdatosComp->esfera=esfera;

	if (pdatosComp->accion==-1) OnKeyboardDown('s',0,0);
	else if (pdatosComp->accion==0){}
	else if (pdatosComp->accion==1) OnKeyboardDown('w',0,0);

}

void CMundoCliente::OnKeyboardDown(unsigned char key, int x, int y)
{
	write(fd_cliente_servidor,&key,sizeof(key));
}

void CMundoCliente::Init()
{
	unlink("/tmp/fifo_servidor_cliente");	
	
	mkfifo("/tmp/fifo_servidor_cliente", 0666);
	fd_servidor_cliente=open("/tmp/fifo_servidor_cliente", O_RDONLY);
	if (fd_servidor_cliente < 0) perror("Error creacion fifo S-C en Cliente");
	
	mkfifo("/tmp/fifo_cliente_servidor", 0666);
	fd_cliente_servidor=open("/tmp/fifo_cliente_servidor", O_WRONLY);
	if (fd_cliente_servidor < 0) perror("Error creacion fifo C-S en Cliente");

	int fd_memcomp;
	fd_memcomp=open("/tmp/datosComp", O_CREAT|O_TRUNC|O_RDWR, 0666);
	datosComp.raqueta1=jugador1;
	datosComp.esfera=esfera;
	datosComp.accion=0;
	
	write(fd_memcomp,&datosComp,sizeof(datosComp));
	pdatosComp=static_cast<DatosMemCompartida*>(mmap(NULL,sizeof(datosComp), PROT_READ|PROT_WRITE, MAP_SHARED, fd_memcomp, 0));
	close(fd_memcomp);

	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
}
