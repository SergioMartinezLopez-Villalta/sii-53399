#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fstream>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include"DatosMemCompartida.h"

int main(void){
	int fd;
	DatosMemCompartida *pdatosComp;
	fd=open("datosMemComp",O_RDWR);
	pdatosComp=static_cast<DatosMemCompartida*>(mmap(0,sizeof(DatosMemCompartida), PROT_READ|PROT_WRITE, MAP_SHARED, fd,0));	

	while(1){
		if(pdatosComp->esfera.centro.y<pdatosComp->raqueta1.getCentro().y){
			pdatosComp->accion=-1;
		}
		else if(pdatosComp->esfera.centro.y==pdatosComp->raqueta1.getCentro().y){
			pdatosComp->accion=0;
		}
		else if(pdatosComp->esfera.centro.y>pdatosComp->raqueta1.getCentro().y){
			pdatosComp->accion=1;
		}	
		usleep(25000);
	}

	munmap(pdatosComp, sizeof(DatosMemCompartida));

	return 0;
}
