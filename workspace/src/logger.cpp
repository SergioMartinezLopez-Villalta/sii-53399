#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h> 
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

int main (void) {

	int fd, puntuacion[2];

	if (mkfifo("/tmp/fifo", 0777) == -1){
		perror("Creacion Fifo");
		return 1;
	}

	//fd = open("fifo", O_RDONLY);

	if((fd = open("/tmp/fifo", O_RDONLY)) == -1){
	 	perror("Apertura Fifo");
		return 1;
	}

	while (read(fd, puntuacion, sizeof(puntuacion)) == sizeof(puntuacion)){
		if (puntuacion[1] == 2) 
			printf("Jugador 2 marca 1 punto, lleva %d puntos.\n",puntuacion[0]);
		else printf("Jugador 1 marca 1 punto, lleva %d puntos.\n",puntuacion[0]);
	}

	close(fd);
	unlink("/tmp/fifo");
	return 0;
}
